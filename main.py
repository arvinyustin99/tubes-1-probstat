'exec(%matplotlib inline)'

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


#dataset_1
data_1 = pd.read_csv("dataset/fifa.csv")
sns.set_style('darkgrid')
#data_1['Age'].hist()
#plt.show()

data_1.boxplot(by = 'Nationality', column = ['Age'], grid = False)
plt.show()