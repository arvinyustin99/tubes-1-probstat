import pandas as pd
import matplotlib.pyplot as plt
'exec(%matplotlib inline)'
import math

data = pd.read_csv("dataset/fifa.csv")

data_AGE = data['Age']

def PX(x):
	count  = 0
	for i in range(0, data_AGE.count()):
		if (data_AGE[i] < x):
			count += 1
	return count

def nCr(n, r):
	nPr = math.factorial(n)//math.factorial(n-r)
	return nPr//math.factorial(r)

def h(x,N,n,k):
	pemb = 0.0
	peny = 0.0
	pemb = nCr(k, x) * nCr(N-k, n-x)
	peny = nCr(N, n)
	return pemb/peny

Tes = PX(22)
print(Tes)
Banyak = data_AGE.count()
total = 0.0
for i in range(0, 1001):
	total += i*h(i, Banyak, 1000, Tes)
print(total)
